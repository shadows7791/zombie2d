﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class GameLogic : MonoBehaviour
{
    public Image[] bttnImg;

    public Sprite bttnNoneTr;

    public Sprite[] textur;

    public Sprite noneTr;

    private int[] rndTextur;

    public bool[] bttnEn;

    //Menu
    public GameObject Pmenu;
    public Text txtWin;
    public Text txtScore;
    private int score = 100;

    private int press1 = 0, press2 = 0;
    private int tempClick1, tempClick2;
    public bool click = false;

    private int cnt = 0;

    public int bttnDontEn = 16;
    private bool iniat = false;

    AudioSource m_MyAudioSource;

    // Start is called before the first frame update
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();

        rndTextur = new int[bttnImg.Length];
        bttnEn = new bool[bttnImg.Length];

        Iniat();
    }

  public  void Iniat()
    {

        if (PlayerPrefs.HasKey("ScoreMemory"))
            score = PlayerPrefs.GetInt("ScoreMemory");


      
   //     const int SIZE = 10;
      int[] masiv = new int[bttnImg.Length];
      System.Random r = new System.Random();
        int i = 0;
        while (i < masiv.Length)
        {
            int temp = r.Next(masiv.Length);
         if (masiv[temp] == 0)
              {
               
                masiv[temp] = i % bttnImg.Length / 2 ;
                ++i;
              }
          
        }
                

        for (i = 0; i < bttnImg.Length; ++i)
        {
          
            rndTextur[i] = masiv[i];

            bttnImg[i].sprite = textur[masiv[i]];
        }

        Invoke("CloseTextur", 2f);
    }

    void CloseTextur()
    {
        for (int i = 0; i < bttnImg.Length; ++i)
        {
            bttnImg[i].sprite = bttnNoneTr;
            bttnEn[i] = false;
        }

        iniat = true;
        bttnDontEn = bttnImg.Length;
        cnt = 0;
    }

   

    // Update is called once per frame
   public void OnClik()
    {
        if (!iniat)
            return;


        GameObject go = EventSystem.current.currentSelectedGameObject;

      int onClickBttn=int.Parse( go.name.Replace("Button",""));

        if (bttnEn[onClickBttn] == true)
            return;

        m_MyAudioSource.Play();


        if (!click)
        {
            press1 = rndTextur[onClickBttn];

            bttnEn[onClickBttn] = true;

            tempClick1 = onClickBttn;

            click = true;


        }
        else
        {
            press2 = rndTextur[onClickBttn];
            click = false;

            tempClick2 = onClickBttn;

            bttnEn[onClickBttn] = true;



            if (press1 != press2)
            {


                cnt++;

                Invoke("EraseTextur", 0.1f);

            }
            else
            {
                bttnDontEn -= 2;

                if (bttnDontEn == 0)     //you win
                {
                    if (score > cnt)
                    {
                        score = cnt;
                        PlayerPrefs.GetInt("ScoreMemory", score);

                    }

                    Pmenu.SetActive(true);
                    txtWin.text = "You win!!! " + cnt + " clicks";
                    txtScore.text = "Score " + score + " clicks";
                }
            }


        }

      

      //  Debug.Log(onClickBttn +"{0}  " + rndTextur[onClickBttn]);

        bttnImg[onClickBttn].sprite = textur[rndTextur[onClickBttn]];

       
    }

    void EraseTextur()
    {

        bttnEn[tempClick1] = false;
        bttnEn[tempClick2] = false;

        bttnImg[tempClick1].sprite = bttnNoneTr;
        bttnImg[tempClick2].sprite = bttnNoneTr;
    }

    public void Play()
    {
        Pmenu.SetActive(false);
        Iniat();
    }

    public void Exit()
    {
        Application.Quit();
    }

    //bool menu = false;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
