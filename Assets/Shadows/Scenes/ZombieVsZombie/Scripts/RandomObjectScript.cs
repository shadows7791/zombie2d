﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectScript : MonoBehaviour
{
    public GameObject[] rndObject;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < rndObject.Length; i++)
        {
            rndObject[i].SetActive(false);
        }

        rndObject[Random.Range(0,rndObject.Length)].SetActive(true);

    }

  
}
