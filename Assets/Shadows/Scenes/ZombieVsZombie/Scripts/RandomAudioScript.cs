﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomAudioScript : MonoBehaviour {

    public AudioClip[] otherClip; //make an arrayed variable (so you can attach more than one sound)
    public AudioSource sound;

    void Start()
    {
       
         sound = gameObject.GetComponent<AudioSource>();

        StartCoroutine("PlaySound");


    }

    // Play random sound from variable
    IEnumerator PlaySound()
    {
        //  Debug.Log("Play");

        //Assign random sound from variable
        AudioClip randomClip= otherClip[Random.Range(0, otherClip.Length - 1)];

        sound.clip = randomClip;

        sound.Play();

        // Wait for the audio to have finished
        yield return new WaitForSeconds(randomClip.length);

        //Now you should re-loop this function Like
        StartCoroutine("PlaySound");
    }
}
