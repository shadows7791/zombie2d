﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinHeadAnimScript : MonoBehaviour
{
    public Sprite[] head;
    public float speed = 1.1f;
    int cntHead = 0;

    // Start is called before the first frame update
    void Start()
    {
     //   Invoke("HeadReplase", speed);
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        if (cntHead < head.Length)
            cntHead++;
        else
            cntHead = 0;

        GetComponent<Image>().sprite = head[cntHead];

     //   Invoke("HeadReplase", speed);
    }
}
