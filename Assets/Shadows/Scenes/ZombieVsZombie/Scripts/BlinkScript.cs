﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkScript : MonoBehaviour
{

    public float speed = 1.1f;

    bool en = true;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LinghtEnable", Random.Range(.5f, speed));
    }
   
    void LinghtEnable()
    {
        en = !en;

        GetComponent<Light>().enabled = en;
        Invoke("LinghtEnable", Random.Range(.5f,speed));
    }


}
