﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieScript : MonoBehaviour
{
    public GameObject zombieDie;

   

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Saw")
        {
            float zombiePos = gameObject.transform.position.x;
            Debug.Log("Zombie   111" + zombiePos);

            if (zombiePos < 0)
                GameObject.Find("GameLogic").GetComponent<GameLogicScript>().UIZombie("Blue");
            else
                GameObject.Find("GameLogic").GetComponent<GameLogicScript>().UIZombie("Red");

            GameObject zd = Instantiate(zombieDie, new Vector3(other.gameObject.transform.position.x, gameObject.transform.position.y + 0.5f, 0), gameObject.transform.rotation);


           


            Destroy(zd, 2f);
            Destroy(gameObject);

        }

        // Debug.Log("Zombie   111" + other.gameObject.tag);
    }

}
