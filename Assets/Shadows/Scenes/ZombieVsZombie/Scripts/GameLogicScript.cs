﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogicScript : MonoBehaviour
{

    //
    public Image[] RedZombie, BlueZombie;

    public GameObject PWin,PLose,PPause;

    public bool win = false;

    public int redCnt = 0, blueCnt = 0;

    private bool pause = false;
   
    public void UIZombie(string _name)
    {
        if (_name == "Blue")
        {
            
            blueCnt++;
            BlueZombie[blueCnt-1].GetComponent<Image>().color = Color.white;
        }

     
        if (_name == "Red")
        {
            redCnt++;
            RedZombie[redCnt-1].GetComponent<Image>().color = Color.white;
           
        }

        if (redCnt >= 3)
        {
            Invoke("EndGame", 2f);
        }

        if (blueCnt >= 3)
        {
            win = true;
            Invoke("EndGame", 2f);
        }

    }

    void EndGame()
    {
        if (win)
        {
            Debug.Log("You win");
            PWin.SetActive(true);
        }
        else
        {
            Debug.Log("You lose");
            PLose.SetActive(true);
        }
        Time.timeScale = 0;
    }

  public  void Play()
    {
        Time.timeScale = 1;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Exit()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    void Pause()
    {
        pause = !pause;

        if (pause)
                Time.timeScale = 0;
          else
            Time.timeScale = 1;

        PPause.SetActive(pause);
    }

    public void Resume ()
    {
        PPause.SetActive(false);
        Time.timeScale = 1;
        pause = false;
    }
}
